using NUnit.Framework;
using FluentAssertions;

namespace test_project.tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Return_Test_String()
        {
            Greeter greeter = new Greeter();
            greeter.Greet().Should().Be("Hello");
        }
    }
}